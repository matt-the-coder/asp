﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewsInDetail.Controllers
{
	public class HomeController : Controller
	{
		//
		// GET: /Home/
		public ActionResult Index()
		{
			string[] fruits = { "Apple", "Orange", "Pear" };
			return View(fruits);
		}

		public ActionResult MyAction()
		{
			string[] fruits = { "Apple", "Orange", "Pear" };
			return View(fruits);
		}
		//how can we add dynamic contents to our views? :
		//inline code            
		//section : can be placed in specific parts of Layouts
		//partialView: to hold the shared parts of other views. it is not possible to add business logic in partialViews.
		//child action: to create resuable gui. They call some action methods, render a view, and the results are placed in response stream.
		//html helper method

		public ActionResult MyAction2()
		{
			return View();
		}

		[ChildActionOnly]
		public ActionResult Time()
		{
			return PartialView(DateTime.Now);
		}
		
	}
}
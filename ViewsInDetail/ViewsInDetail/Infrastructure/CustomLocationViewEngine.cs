﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewsInDetail.Infrustructure
{
    public class CustomeLocationViewEngine:RazorViewEngine
    {
        public CustomeLocationViewEngine()
        {
            // {1} for controller name   and {0} for view name
            ViewLocationFormats = new string[] { "~/Views/{1}/{0}.cshtml" ,"~/Views/Common/{0}.cshtml"};
        }
    }
}
﻿using RoutesAndUrls.Infrustructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Routing.Constraints;
using System.Web.Routing;

namespace RoutesAndUrls
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //Route myRoute = new Route("{controller}/{action}", new MvcRouteHandler());
            //routes.Add("MyRoute", myRoute);

            //routes.MapRoute("MyRoute", "{controller}/{action}");

            //routes.MapRoute("MyRoute", "{controller}/{action}", new { action="Index" ,controller="Home"});
            //http://mydomain/Public/Home/Index

            //routes.MapRoute("MyRoute", "Public/{controller}/{action}", new { action = "Index", controller = "Home" });

            //routes.MapRoute("MyRoute2", "X{controller}/{action}", new { action = "Index", controller = "Home" });

            //routes.MapRoute("MyRoute", "{controller}/{action}", new { action = "Index", controller = "Home" });

            //routes.MapRoute("ShopSchema", "Shop/{action}", new { controller = "Home" });

            //routes.MapRoute("ShopSchema", "Shop/OldAction", new { controller = "Home",action="Index" });

            //routes.MapRoute("MyRoute", "{controller}/{action}/{id}", new { controller = "Home", action = "Index", id = "DefaultId" });

            //routes.MapRoute("MyRoute", "{controller}/{action}/{id}/{*catchall}", new { controller = "Home", action = "Index", id = UrlParameter.Optional });
            //http://mydomain/home/index/2/Delete    catchall=Delete
            //http://mydomain/home/index/2/Delete/param   catchall = Delete/param   catchall.splite('/')

            //routes.MapRoute("MyRoute", "{controller}/{action}/{id}/{*catchall}",
            //    new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //    , new[] { "RoutesAndUrls.Controllers" }
            //    );

            //Route MyRoute = routes.MapRoute("MyRoute", "{controller}/{action}/{id}/{*catchall}",
            //  new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //  , new[] { "RoutesAndUrls.AdditionalControllers" }
            //  );

            //MyRoute.DataTokens["UseNameSpaceFallback"] = false;

            //routes.MapRoute("MyRoute", "{controller}/{action}/{id}/{*catchall}",
            //  new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //  , new { controller="^H.*",action ="^Index$|^About$"}
            //  , new[] { "RoutesAndUrls.AdditionalControllers" }
            //  );


            //routes.MapRoute("MyRoute", "{controller}/{action}/{id}/{*catchall}",
            //new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //, new { controller = "^H.*", action = "^Index$|^About$" , httpMethod=new HttpMethodConstraint("GET","POST") }
            //, new[] { "RoutesAndUrls.AdditionalControllers" }
            //);


            // routes.MapRoute("MyRoute", "{controller}/{action}/{id}/{*catchall}",
            //new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //, new { controller = "^H.*", action = "^Index$|^About$", httpMethod = new HttpMethodConstraint("GET", "POST"), id=new RangeRouteConstraint(10,20) }
            //, new[] { "RoutesAndUrls.AdditionalControllers" }
            //);

            //AlphaRouteConstraint()
            //BoolRouteConstraint()  DoubleRouteConstraint()
            //LengthRouteConstraint(len)  LengthRouteConstraint(min,max)

            // routes.MapRoute("MyRoute", "{controller}/{action}/{id}/{*catchall}",
            //new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //, new { controller = "^H.*", action = "^Index$|^About$", httpMethod = new HttpMethodConstraint("GET", "POST"), id = new CompoundRouteConstraint(new IRouteConstraint[]{ new AlphaRouteConstraint(),new MinLengthRouteConstraint(6)}) }
            //, new[] { "RoutesAndUrls.AdditionalControllers" }
            //);


            //routes.MapRoute("MyRoute", "{controller}/{action}/{id}/{*catchall}",
            //new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //, new { customeConstraint = new UserAgentConstraint("chrome") }
            //, new[] { "RoutesAndUrls.AdditionalControllers" }
            //);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RoutesAndUrls.Controllers
{
    //[RoutePrefix("Users")]
    public class CustomerController : Controller
    {
        //
        // GET: /Customer/
        public ActionResult Index()
        {
            ViewBag.Controller = "Customer";
            ViewBag.Action = "Index";

            return View("ActionName");
        }

        //[Route("~/Test")]
        //[Route("~/Test2")]
        [Route("Test")]
        [Route("Test2")]
        public ActionResult ChangePassword()
        {
            ViewBag.Controller = "Customer";
            ViewBag.Action = "ChangePassword";

            return View("ActionName");
        }
        [Route("Users/Add/{user}/{id:int}")]
        public string Create(string user,int id)
        {
            return string.Format("Username is:{0} , Id is: {1}", user, id);
        }

        [Route("Users/Add/{user}/{password:alpha:length(6)}")]
        public string ChangePassword(string user, string password)
        {
            return string.Format("Username is:{0} , password is: {1}", user, password);
        }


        public ActionResult Links()
        {
            return View();
        }

        public RedirectToRouteResult MyActionMethod()
        {
            return RedirectToAction("Index","Customer",null);
        }

        public RedirectToRouteResult MyActionMethod2()
        {
            return RedirectToRoute(new { controller = "Home", action = "Index"});
        }
	}
}
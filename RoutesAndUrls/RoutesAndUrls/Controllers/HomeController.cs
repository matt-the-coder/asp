﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RoutesAndUrls.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            ViewBag.Controller = "Home";
            ViewBag.Action = "Index";

            return View("ActionName");
        }

        public ActionResult ChangePassword()
        {
            ViewBag.Controller = "Home";
            ViewBag.Action = "ChangePassword";

            return View("ActionName");
        }

        public ActionResult CustomeVariable()
        {
            ViewBag.Controller = "Home";
            ViewBag.Action = "CustomeVariable";

            ViewBag.CustomeVariable = RouteData.Values["id"];

            return View("ActionName");
        }

        public ActionResult CustomeVariableWithParameter(string id)
        {
            ViewBag.Controller = "Home";
            ViewBag.Action = "CustomeVariable";

            ViewBag.CustomeVariable = id??"<no Value>";

            return View("ActionName");
        }
	}
}
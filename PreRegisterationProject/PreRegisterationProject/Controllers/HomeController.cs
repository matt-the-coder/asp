﻿using PreRegisterationProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreRegisterationProject.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Registeration()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Registeration(RegisterationInfo formData)
        {
            //Save to Db
            //Email
            if (ModelState.IsValid)
                return View("Thanks", formData);
            else
                return View();
        }
    }
}
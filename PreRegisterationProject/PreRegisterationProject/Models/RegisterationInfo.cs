﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PreRegisterationProject.Models
{
    public class RegisterationInfo
    {
        [Required(ErrorMessage = "Please Enter Full Name")]
        public string FullName { get; set; }
        [Required(ErrorMessage = "Please Enter your Email")]
        [RegularExpression(".+\\@.+\\..+",ErrorMessage="Please Enter a Valid Email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please Enter your Phone Number")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Please Select an option from Send Notification DropDown List")]
        public bool? SendNotification { get; set; }
    }
}
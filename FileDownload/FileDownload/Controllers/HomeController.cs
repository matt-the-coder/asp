﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FileDownload.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult DownloadFile()
        {
            return new FilePathResult(@"~\Downloads\myFile.docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }
	}
}
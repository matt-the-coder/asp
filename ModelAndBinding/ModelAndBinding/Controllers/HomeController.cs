﻿using ModelAndBinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ModelAndBinding.Controllers
{
    public class HomeController : Controller
    {
        private Person[] personData = {
            new Person {PersonId = 1, FirstName = "John", LastName = "Abbott",Role = Role.Admin},
            new Person {PersonId = 2, FirstName = "Isaac", LastName = "Newton",Role = Role.User},
            new Person {PersonId = 3, FirstName = "Bill", LastName = "Gates",Role = Role.User},
            new Person {PersonId = 4, FirstName = "Nelson", LastName = "Mandela",Role = Role.Guest}
        };
        // GET: Home
        public ActionResult Index(int id=1)
        {
            Person dataItem = personData.Where(p => p.PersonId == id).First();
            return View(dataItem);
        }
        //Bindings are based on theses context objects, in order as follow:
            //Search Order:
            //Request.Form["id"]
            //RoutData.Values["id"]
            //Request.QueryString["id"]
            //Request.Files["id"]

        //We can have a complex iput parameter. int he following actions, the inout is of type Person class
        public ActionResult CreatePerson()
        {
            return View(new Person());
        }
        [HttpPost]
        public ActionResult CreatePerson(Person model)
        {
            return View("Index", model);
        }

        //public ActionResult DisplaySummary(AddressSummary summary)
        //{
        //    return View(summary);
        //}

        //This action does NOT populate the values of City and Country. it should be 
        public ActionResult DisplaySummary(AddressSummary summary)
        {
            return View(summary);
        }

        //public ActionResult DisplaySummary([Bind(Prefix = "HomeAddress")]AddressSummary summary)
        //{
        //    return View(summary);
        //}


        //public ActionResult DisplaySummary([Bind(Prefix = "HomeAddress", Exclude = "Country")]AddressSummary summary)
        //{
        //    return View(summary);
        //}

        public ActionResult Names(string[] names)
        {
            names = names ?? new string[0];
            return View(names);
        }

        public ActionResult Address(IList<AddressSummary> addresses)
        {
            addresses = addresses ?? new List<AddressSummary>();
            return View(addresses);
        }

        //public ActionResult Address()
        //{
        //    IList<AddressSummary> addresses = new List<AddressSummary>();
        //    UpdateModel(addresses, new FormValueProvider(ControllerContext));//restricted to Request.Form
        //    return View(addresses);
        //}

        //Request.Form ---> FormValueProvider
        //RouteData.Values ----> RouteDataValueProvider
        //Request.QueryString ----> QueryStringValueProvider
        //Request.Files ----> HttpFileCollectionValueProvider

        //public ActionResult Address(FormCollection formData)//equals to previous action
        //{
        //    IList<AddressSummary> addresses = new List<AddressSummary>();
        //    try
        //    {
        //        UpdateModel(addresses, formData);
        //    }
        //    catch (InvalidOperationException ex)
        //    {
        //        // provide feedback to user
        //    }
        //    return View(addresses);
        //}



    }
}
﻿using BuiltInHelperMethods.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuiltInHelperMethods.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult CreatePerson()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult CreatePerson(Person person)
        {
            //process model
            return View(person);
        }


        public ActionResult CreatePersonWithHelperMethods()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult CreatePersonWithHelperMethods(Person person)
        {
            //process model
            return View(person);
        }
	}
}
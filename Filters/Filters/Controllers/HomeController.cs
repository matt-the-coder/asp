﻿using Filters.Infrustructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [CustomAuth(false)]
        public string Index()
        {
            return "This is the Index action on the Home controller";
        }

        [MySiteAuth]
        //[Authorize(Users="john@mysite.com")]
        public string List()
        {
            return "this is the list action on the Home controller";
        }

        [RangeException]
        public string RangeTest(int id)
        {
            if (id > 100)
            {
                return String.Format("The id value is: {0}", id);
            }
            else
            {
                throw new ArgumentOutOfRangeException("id", id, "");
            }
        }

        [ProfileAction]
        [ProfileResult]
        public string FilterTest()
        {
            return "This is the ActionFilterTest action";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;
using System.Web.Security;

namespace Filters.Infrustructure
{
    public class MySiteAuthAttribute: FilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            IIdentity ident = filterContext.Principal.Identity;
            if (!ident.IsAuthenticated || !ident.Name.EndsWith("@mysite.com"))
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if(filterContext.Result==null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {

                    {"controller","MySiteAccount"},
                    {"action","Login"},
                    {"returnUrl",filterContext.HttpContext.Request.RawUrl}
                });
            }
            else
            {
                FormsAuthentication.SignOut();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.Infrustructure
{
    public class ProfileResultAttribute:FilterAttribute,IResultFilter
    {
        private Stopwatch timer;
        public void OnResultExecuting(ResultExecutedContext filterContext)
        {
            timer= Stopwatch.StartNew();
        }

        public void OnResultExecuted(ResultExecutingContext filterContext)
        {
            timer.Stop();
            filterContext.HttpContext.Response.Write(
                string.Format("<div>Result elapsed time: {0:F6}</div>",
                    timer.Elapsed.TotalSeconds)
                );
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            //throw new NotImplementedException();
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            //throw new NotImplementedException();
        }
    }
}
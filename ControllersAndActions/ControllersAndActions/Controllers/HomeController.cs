﻿using ControllersAndActions.Infrustructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ControllersAndActions.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        //Query String: www.domain.com/Home/Index?name=john&family=abbott&age=20
        public ActionResult Index()
        {
            //how to retrive the data of user's request:
            //Request.QueryString
            //Request.Form
            //Request.HttpMethod
            //Request.Url
            //RouteData.Route
            //RouteData.Values
            //HttpContext.Session
            //User
            //TempData

            string username = User.Identity.Name;            
            string serverName = Server.MachineName;
            string clientIp = Request.UserHostAddress;

            string name = Request.Form["Name"];

            return View();
        }

        public ActionResult MyAction(string name)
        {
            return View();

        }

        public ActionResult MyAction2()
        {
            string name = (string)RouteData.Values["name"];
            return View();
        }

        public ActionResult MyAction3(int id=0)
        {
            return View();
        }

        //For output data
        public void MyAction4()
        {
            HttpContext.Response.Write("Hello From HttpContext.Response");
            HttpContext.Response.Write("Hello 2");
        }

        //To define a customized Action Result
        public ActionResult MyAction5()
        {
            //to protect some data while redirecting:
            //TempData["Message"] = "hello";
            TempData["Day"] = DateTime.Now.ToString();

            return new myCustomeRedirectResult { URL = "/Home/Index" };   
        }

        //list of some pre-define ActionResults in ASP.NET MVC:
        //FileResult
        //ViewResult
        //HttpNotFoundResult
        //HttpStatusCodeResult
        //HttpUnauthoriedResult
        //RedirectResult
        //ContentResult


        //How to send data from Action to View :  1) by passing Model (typed and untyped)  2) using ViewBag

        public ActionResult UntypedView()
        {
            DateTime date = new DateTime(2017, 1, 1, 0, 0, 0);
            return View(date);
        }

        public ActionResult StronglyTypedView()
        {
            DateTime date = new DateTime(2017, 1, 1, 0, 0, 0);
            return View(date);
        }

        public ActionResult ViewBagExample()
        {
            DateTime date = new DateTime(2017, 1, 1, 0, 0, 0);
            ViewBag.Date = date;
            return View();
        }

	}
}
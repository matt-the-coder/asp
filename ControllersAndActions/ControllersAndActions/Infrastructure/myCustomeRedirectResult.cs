﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ControllersAndActions.Infrustructure
{
    public class myCustomeRedirectResult:ActionResult
    {
        public string URL { get; set; }
        public override void ExecuteResult(ControllerContext context)
        {
            string fullURL = UrlHelper.GenerateContentUrl(URL, context.HttpContext);
            context.HttpContext.Response.Redirect(fullURL);
        }
    }
}
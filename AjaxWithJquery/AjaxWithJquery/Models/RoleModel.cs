﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxWithJquery.Models
{
    public class RoleModel
    {
        public string RoleName { get; set; }
        public string Description { get; set; }
    }
}
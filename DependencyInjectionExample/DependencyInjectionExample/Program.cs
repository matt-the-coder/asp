﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjectionExample
{
    class Program
    {
        static void Main(string[] args)
        {
            PasswordHelper pass_helper = new PasswordHelper(new EmailSender());//product

            pass_helper.setEmailSender(new FakeEmailSender());//setter injection

            PasswordHelper pass_helper2 = new PasswordHelper(new FakeEmailSender());//test
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjectionExample
{
    public class PasswordHelper
    {
        private IEmailSender email_sender;
        public PasswordHelper(IEmailSender sender)//constructor injection
        {
            email_sender = sender;
        }

        public void setEmailSender(IEmailSender sender)
        {
            email_sender = sender;
        }
        public void ResetPassword()
        {

            //some implementation

            email_sender.Send();
        }
    }
}

﻿using EF_DatabaseFirst.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_DatabaseFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            using(UniversityEntities context = new UniversityEntities())
            {
                //LINQ : Language Integrated Query
                var city = context.Cities.FirstOrDefault(x => x.Id == 1);

                Console.WriteLine("City name is:" + city.Name);
                Console.ReadLine();


                var cities = context.Cities.Where(x => x.Name.StartsWith("Lo")).OrderByDescending(x => x.Id).ToList();

                //foreach (City _city in cities)
                //{
                //    Console.WriteLine(_city.Name);
                //}



                //add object to db
                //context.Cities.Add(new City
                //{
                //    Name = "Aachen"
                //});

                //context.SaveChanges();

                //remove
                //var a = context.Cities.FirstOrDefault(x => x.Name == "Montreal");
                //if(a!=null)
                //{
                //    context.Cities.Remove(a);
                //    context.SaveChanges();
                //}


                //edit
                //var laval_city = context.Cities.FirstOrDefault(x => x.Name == "Laval");
                //if(laval_city!=null)
                //{
                //    laval_city.Name = "Aachen";
                //    context.SaveChanges();
                //}


                Console.ReadLine();

            }
        }
    }
}

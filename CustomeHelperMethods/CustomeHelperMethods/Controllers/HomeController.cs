﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomeHelperMethods.Controllers
{
	public class HomeController : Controller
	{
		//
		// GET: /Home/
		public ActionResult Index()
		{
			ViewBag.Fruits = new string[] { "Apple", "Orange", "Pear" };
			ViewBag.Cities = new string[] { "Montreal", "Sherbrook", "Quebec", "Laval" };
			return View();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomeHelperMethods.Infrustructure
{
    public static class CustomeHelperMethod
    {
        //to define an Extension Method:
        //Extension methods are used in case that we plan to extend a class that has been developed by others and 
        //we have no access to its code but we want to add a method to that class.
        //Here we use MvcHtmlString. As first argument, we use 'this', then the class name which is 'HtmlHelper' then a name for the object.
        //it means that the method 'ListArrayItems' is callable on the objects of the class HtmlHelper.
        //The input of the method is 'string[] list'. That is the only input.        
        public static MvcHtmlString ListArrayItems(this HtmlHelper html,string[] list)
        {
            TagBuilder tag = new TagBuilder("ul");
            foreach(string str in list)
            {
                TagBuilder itemTag = new TagBuilder("li");
                itemTag.SetInnerText(str);
                tag.InnerHtml += itemTag.ToString();
                //tag.AddCssClass                  
               
            }

            return new MvcHtmlString(tag.ToString());
        }
    }
}
﻿using EF_CodeFirst.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_CodeFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            using(var context = new MyContext())
            {
                context.Students.Add(new Student
                {
                    Name = "Isaac",
                    Family = "Newton",
                    NationalCode="123456788",
                    
                });

                context.SaveChanges();
            }
        }
    }
}
